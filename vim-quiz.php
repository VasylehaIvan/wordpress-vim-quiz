<?php
/**
 * Created by PhpStorm.
 * User: ivan
 * Date: 11.03.17
 * Time: 21:17
 */
/*
Plugin Name: Wordpress Quiz
Author: Ivan
Author URI: https://www.linkedin.com/messaging/thread/6238311779009724416/
*/

define('VIM_ROOT_DIR', str_replace('\\', '/', dirname(__FILE__)));
define('VIM_ROOT_URL', rtrim(plugin_dir_url(__FILE__), '/'));
include_once 'class/quiz.php';