/**
 * Created by ivan on 11.03.17.
 */
"use strict";
jQuery(document).ready(function($){
    var forms = $('.quiz-shortcode');

    forms.on('submit', function(event){
        event.preventDefault();
        $.post(
            ajaxurl,
            {
                action: 'quiz_data',
                _ajax_nonce: $('._ajax_nonce', this).val(),
                form_data: $(this).serialize()
            },
            function($response){
                alert($response);
                /*location.reload();*/
            }
        )
    });
});