/**
 * Created by ivan on 12.03.17.
 */
"use strict";
jQuery(document).ready(function($) {
    var questionsBox = $('#questions-box');

    questionsBox.on('click', '.add_question', function () {
        var id_question = isNaN($('.list-questions > li', questionsBox).data('id')) ? 0 : +$('.list-questions > li', questionsBox).last().data('id') + 1;

        var question = $('#question').html()
            .replaceAll('%%id_question%%', id_question)
            .replaceAll('%%value%%', '')
            .replaceAll('%%answer%%', '');

        $('.list-questions', questionsBox).append($(question));
        return false;
    });

    questionsBox.on('click', '.add_answer', function () {
        var listQuestion = $($(this).parent());
        var id_answer = isNaN($('.answer input[type=radio]', listQuestion).val()) ? 0 : +$('.answer input[type=radio]', listQuestion).last().val() + 1;

        var answer = $('#answer').html()
            .replaceAll('%%id_question%%', $(listQuestion).data('id'))
            .replaceAll('%%id_answer%%', id_answer)
            .replaceAll('%%value%%', '')
            .replaceAll('%%checked%%', id_answer === 0 ? 'checked="checked"' : '');

        $('.answer', $(this).parent()).append($(answer));
        return false;
    });
    questionsBox.on('click', '.remove', function () {
        $(this).parent().remove();
        return false;
    });

    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
});