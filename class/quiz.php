<?php
namespace vim;

abstract class Quiz {
	private static $ver = '0.1';
	private static $view_data = [];
	private static $post_type = 'wordpress_quiz';
	public static function init() {
		add_action( 'init', [ __CLASS__, 'custom_post_type' ] );
		add_shortcode( self::$post_type, [__CLASS__, 'quiz_shortcode'] );

		if (is_admin()){
			add_action('admin_init', [__CLASS__, 'admin_init'] );
			add_action( 'add_meta_boxes', [ __CLASS__, 'add_meta_boxes' ] );
			add_action('save_post_'.self::$post_type, [__CLASS__, 'save_meta_boxes']);
			add_action( 'manage_'.self::$post_type.'_posts_custom_column', [__CLASS__, 'manage_column'], 10, 2 );
			add_filter( 'manage_'.self::$post_type.'_posts_columns', [ __CLASS__, 'manage_columns' ],1000 );
		} else {
			add_action('init', [__CLASS__, 'wp_init'] );
		}

		add_action('wp_ajax_quiz_data', [__CLASS__, 'ajax_quiz']);
		add_action('wp_ajax_nopriv_quiz_data', [__CLASS__, 'ajax_quiz']);
	}

	public static function quiz_shortcode($atts) {
		shortcode_atts( [ 'id' => 0 ], $atts );
		$response = '';

		if ( get_post_type( $atts['id'] ) === self::$post_type ) {
			wp_enqueue_script('wordpress_quiz.js', VIM_ROOT_URL.'/assets/js/wordpress_quiz.js', ['jquery'], self::$ver, true);
			wp_localize_script('wordpress_quiz.js', 'ajaxurl',
				admin_url('admin-ajax.php')
			);

			ob_start();

			self::include_template( 'quiz_shortcode', [
				'quiz_data' => get_post_meta( $atts['id'], 'vim_quiz_data', true ),
				'id'        => $atts['id']
			] );

			$response = ob_get_clean();
		}

		return $response;
	}

	public static function ajax_quiz() {
		parse_str( $_POST['form_data'] );
		$post_id = ! empty( $post_id ) ? $post_id : 0;

		check_ajax_referer( 'quiz_data_' . $post_id );

		$quiz_data = get_post_meta( $post_id, 'vim_quiz_data', true );

		$correct_answer = 0;
		foreach ( $quiz_data as $id_question => $question ) {
			if ( empty($question['answers']) || $question['correct_answer'] == $quiz[ $id_question ] ) {
				$correct_answer ++;
			}
		}

		$bad = 0;
		if ( $correct_answer / count( $quiz_data ) >= 0.8 ) {
			$response = 'Congratulations, You passed the quiz successfully.';
		} else {
			$response = 'Unfortunately, You failed the quiz';
			$bad = 1;
		}

		$quiz_stat_data = get_post_meta($post_id, 'quiz_stat_data', true);
		@$quiz_stat_data['count'] += 1;
		@$quiz_stat_data['bad'] += $bad;
		update_post_meta($post_id, 'quiz_stat_data', $quiz_stat_data);

		echo $response;
		die;
	}

	public static function wp_init(){
		wp_enqueue_style('wordpress_quiz.css', VIM_ROOT_URL.'/assets/css/wordpress_quiz.css');
	}

	public static function admin_init(){
		wp_enqueue_style('quiz_admin.css', VIM_ROOT_URL.'/assets/css/admin.css');
	}

	public static function manage_column($column_name, $post_id){
		switch($column_name){
			case 'shortcode':
				echo '<input type="text" value="['.self::$post_type.' id='.$post_id.']" readonly />';
				break;
			case 'count':
			case 'bad':
				$quiz_stat_data = get_post_meta($post_id, 'quiz_stat_data', true);
				echo isset($quiz_stat_data[$column_name])?$quiz_stat_data[$column_name]:0;
				break;
			case 'good':
				$quiz_stat_data = get_post_meta($post_id, 'quiz_stat_data', true);
				echo @$quiz_stat_data['count'] - @$quiz_stat_data['bad'];
				break;
		}
	}

	public static function manage_columns($columns){
		$columns = [
			'cb' => $columns['cb'],
			'title' => $columns['title'],
			'count' => 'Count',
			'bad' => 'Bad',
			'good' => 'Good',
			'shortcode' => 'Shortcode'
		];
		return apply_filters( __METHOD__, $columns );
	}

	public static function questions_box($post) {
		wp_enqueue_script('questions_box.js', VIM_ROOT_URL.'/assets/js/questions_box.js', ['jquery'], self::$ver, true);
		self::$view_data = get_post_meta($post->ID, 'vim_quiz_data', true);

		self::include_template('questions_box', ['id' => $post->ID, 'shortcode' => self::$post_type]);
	}

	public static function view_list_questions() {
		if ( empty( self::$view_data ) ) {
			return;
		}


		$search_answer = [ '%%id_question%%', '%%id_answer%%', '%%value%%', '%%checked%%' ];
		$search_question = [ '%%id_question%%','%%answer%%', '%%value%%' ];
		$question_block = '';

		foreach ( self::$view_data as $id_question => $question_data ) {

			$answer_block = '';
			if ( $question_data['answers'] ) {
				foreach ( $question_data['answers'] as $id_answer => $value ) {
					ob_start();
					self::include_template( 'block/admin_answer' );

					$replace      = [
						$id_question,
						$id_answer,
						$value,
						checked( $id_answer, $question_data['correct_answer'], false)
					];
					$answer_block .= str_replace( $search_answer, $replace, ob_get_clean() );
				}
			}

			ob_start();
			self::include_template( 'block/admin_question' );

			$replace = [$id_question, $answer_block,$question_data['text'] ];
			$question_block .= str_replace( $search_question, $replace, ob_get_clean() );
		}

		echo $question_block;
	}

	public static function add_meta_boxes( $post ) {
		add_meta_box(
			'questions-box',
			__( 'Questions' ),
			[ __CLASS__, 'questions_box' ],
			self::$post_type,
			'normal',
			'default'
		);
	}

	public static function save_meta_boxes( $post_id ) {
		if ( isset( $_POST['question'] ) ) {

			$result = [];
			foreach ( $_POST['question'] as $question ) {
				$answers        = [ ];
				$correct_answer = 0;

				$correct_ = isset( $question['correct_answer'] ) ? $question['correct_answer'] : 0;

				if ( isset( $question['answers'] ) ) {
					foreach ( $question['answers'] as $key => $answer ) {
						if ( ! empty( $answer ) ) {
							$answers[] = $answer;

							if ( $correct_ == $key ) {
								$correct_answer = count( $answers ) - 1;
							}
						}
					}
				}
				if ( ! empty( $question['text'] ) || ! empty( $answers ) ) {
					$result[] = [
						'text'           => trim( $question['text'] ),
						'correct_answer' => $correct_answer,
						'answers'        => $answers,
					];
				}
			}

			update_post_meta( $post_id, 'vim_quiz_data', $result );
		}
	}

	public static function custom_post_type() {

		$labels = [
			'name'                  => _x( 'Quizzes', 'Post Type General Name', self::$post_type ),
			'singular_name'         => _x( 'Quiz', 'Post Type Singular Name', self::$post_type ),
			'menu_name'             => __( 'Quizzes', self::$post_type ),
			'name_admin_bar'        => __( 'Quiz', self::$post_type ),
			'archives'              => __( 'Item Archives', self::$post_type ),
			'attributes'            => __( 'Item Attributes', self::$post_type ),
			'parent_item_colon'     => __( 'Parent Item:', self::$post_type ),
			'all_items'             => __( 'All Items', self::$post_type ),
			'add_new_item'          => __( 'Add New Item', self::$post_type ),
			'add_new'               => __( 'Add New', self::$post_type ),
			'new_item'              => __( 'New Item', self::$post_type ),
			'edit_item'             => __( 'Edit Item', self::$post_type ),
			'update_item'           => __( 'Update Item', self::$post_type ),
			'view_item'             => __( 'View Item', self::$post_type ),
			'view_items'            => __( 'View Items', self::$post_type ),
			'search_items'          => __( 'Search Item', self::$post_type ),
			'not_found'             => __( 'Not found', self::$post_type ),
			'not_found_in_trash'    => __( 'Not found in Trash', self::$post_type ),
			'featured_image'        => __( 'Featured Image', self::$post_type ),
			'set_featured_image'    => __( 'Set featured image', self::$post_type ),
			'remove_featured_image' => __( 'Remove featured image', self::$post_type ),
			'use_featured_image'    => __( 'Use as featured image', self::$post_type ),
			'insert_into_item'      => __( 'Insert into item', self::$post_type ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', self::$post_type ),
			'items_list'            => __( 'Items list', self::$post_type ),
			'items_list_navigation' => __( 'Items list navigation', self::$post_type ),
			'filter_items_list'     => __( 'Filter items list', self::$post_type ),
		];

		$args = [
			'label'               => __( 'Quiz', self::$post_type ),
			'description'         => __( 'Post Type Quiz', self::$post_type ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'show_in_rest'        => false,
		];

		register_post_type( self::$post_type, $args );
	}

	public static function include_template($template, $data=[]) {
		$template_pach = VIM_ROOT_DIR . '/template/' . $template . '.php';
		if (file_exists($template_pach)){
			include $template_pach;
		}
	}
}
Quiz::init();

