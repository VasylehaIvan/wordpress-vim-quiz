<?php if (!is_array($data['quiz_data'])) return;?>
<form class="quiz-shortcode">
	<input type="hidden" name="post_id" value="<?php echo $data['id'];?>"/>
	<input type="hidden" class="_ajax_nonce" value="<?php echo wp_create_nonce( 'quiz_data_'.$data['id'] );?>"/>

	<h2><?php echo get_the_title($data['id']);?></h2>

	<?php foreach($data['quiz_data'] as $id_question => $question):?>
		<p class="question"><?php echo $question['text'];?></p>
		<?php if (empty($question['answers'])):?>
			<ul>
				<li><label>A rhetorical question</label></li>
			</ul>
			<?php continue;?>
		<?php endif;?>
		<ul>
			<?php foreach($question['answers'] as $id_answer => $answer):?>
				<li><label><input type="radio" name="quiz[<?php echo $id_question;?>]" value="<?php echo $id_answer;?>" required="required"/> <?php echo $answer;?></label></li>
			<?php endforeach;?>
		</ul>
	<?php endforeach;?>

	<button type="submit" class="button">Submit</button>
</form>
