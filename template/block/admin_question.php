<li data-id="%%id_question%%">
	<textarea name="question[%%id_question%%][text]" placeholder="Enter question">%%value%%</textarea>
	<ul class="answer">%%answer%%</ul>
	<button class="add_answer button button-primary">Add new answer</button>
	<button class="remove button button-primary">Remove Question</button>
</li>
