<h3>Hortcode:</h3>
<input type="text" value="[<?php echo $data['shortcode'];?> id=<?php echo $data['id'];?>]" readonly/>
<h3>Questions:</h3>
<ol class="list-questions">
	<?php vim\Quiz::view_list_questions();?>
</ol>
<button class="add_question button button-primary">Add new question</button>

<script id="question" type="template">
	<?php vim\Quiz::include_template('block/admin_question');?>
</script>

<script id="answer" type="template">
	<?php vim\Quiz::include_template('block/admin_answer');?>
</script>