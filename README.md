 

1. In the menu will appear item "Quizzes" after installing the plug-in

2. To create a new Quiz, click "WPadmin :: Quizzes-> Add New" 

3. To add a new question, click "Add new question" 

4. To add a reply, click "Add new answer" for the corresponding question 

5. The correct answer can be only one. On the contrary to the right answer you need to select the radio button. 
** PS: If the reply field is empty, this field will be ignored when saving. Also, if the question field is empty, and this question not have by least 1 line with the not-empty answer, this question will be ignored while saving ** 

6. ShortCode can be obtained both in editing each Quiz, and in the list of all Quiz

7. Statistics on the answers can be found in "WPadmin :: Quizzes-> All Items"
